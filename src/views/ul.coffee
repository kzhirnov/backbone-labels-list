class LabelsList.Ul extends LabelsList.View
	tagName : 'ul'

	className : 'list-inline labels-list'

	initialize : (options) ->
		{@filter} = options
		@items = []

		@listenTo @collection, 'sync', @render

	render : ->
		@clearItems()
		@renderCollection()

		return @

	renderCollection : ->
		if @filter
			_.each @collection.where(@filter), (model) =>
				@renderItem model
		else
			@collection.each (model) =>
				@renderItem model

	renderItem : (model) ->
		item = @settings.makeLi {
			model : model
			collection : @collection
		}
		@$el.append item.render().$el
		@items.push item

	remove : ->
		@clearItems()

		return super

	clearItems : ->
		_.each @items, (view, key) ->
			view.remove()

		@items = []