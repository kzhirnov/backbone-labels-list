class LabelsList.Li extends LabelsList.View
	tagName : 'li'

	events : ->
		return {
			"click .rm" : "onRmClicked"
		}

	render : ->
		@$el.html @getTpl()

		return @

	getTitle : ->
		return @model.get 'title'

	getTpl : ->
		out = """
<span class="title">#{@getTitle()}</span>
"""

		if @settings.get('removeLink')
			out += """
<a href="#" class="rm"><span class="glyphicon glyphicon-remove-circle"></span></a>
"""

		return out

	onRmClicked : (e) ->
		e.preventDefault()

		@collection.trigger 'removeClicked', @model