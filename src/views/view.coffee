class LabelsList.View extends Backbone.View
	constructor : (options) ->
		_.extend @, _.pick(options, ['settings'])

		if !(@settings instanceof LabelsList.Settings)
			@settings = new LabelsList.Settings @settings

		super

	remove : ->
		@settings = null
		@model = null
		@collection = null

		return super