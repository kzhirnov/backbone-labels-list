class LabelsList.Settings extends Backbone.Model
	defaults:
		LiConstructor : LabelsList.Li
		removeLink : true

	makeLi : (options = {}) ->
		return @makeViewInstance @get('LiConstructor'), options

	makeViewInstance : (Constructor, options) ->
		_.extend options, {
			settings : @
		}

		return new Constructor(options)
