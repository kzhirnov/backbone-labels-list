(function (root, factory) {

  if (typeof define === "function" && define.amd) {
    // AMD (+ global for extensions)
    define(["underscore", "backbone"], function (_, Backbone) {
      return (root.LabelsList = factory(_, Backbone));
    });
  } else if (typeof exports === "object") {
    // CommonJS
    module.exports = factory(require("underscore"), require("backbone"));
  } else {
    // Browser
    root.LabelsList = factory(root._, root.Backbone);
  }}(this, function (_, Backbone) {

  "use strict";

var LabelsList,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

LabelsList = {};

LabelsList.View = (function(superClass) {
  extend(View, superClass);

  function View(options) {
    _.extend(this, _.pick(options, ['settings']));
    if (!(this.settings instanceof LabelsList.Settings)) {
      this.settings = new LabelsList.Settings(this.settings);
    }
    View.__super__.constructor.apply(this, arguments);
  }

  View.prototype.remove = function() {
    this.settings = null;
    this.model = null;
    this.collection = null;
    return View.__super__.remove.apply(this, arguments);
  };

  return View;

})(Backbone.View);

LabelsList.Li = (function(superClass) {
  extend(Li, superClass);

  function Li() {
    return Li.__super__.constructor.apply(this, arguments);
  }

  Li.prototype.tagName = 'li';

  Li.prototype.events = function() {
    return {
      "click .rm": "onRmClicked"
    };
  };

  Li.prototype.render = function() {
    this.$el.html(this.getTpl());
    return this;
  };

  Li.prototype.getTitle = function() {
    return this.model.get('title');
  };

  Li.prototype.getTpl = function() {
    var out;
    out = "<span class=\"title\">" + (this.getTitle()) + "</span>";
    if (this.settings.get('removeLink')) {
      out += "<a href=\"#\" class=\"rm\"><span class=\"glyphicon glyphicon-remove-circle\"></span></a>";
    }
    return out;
  };

  Li.prototype.onRmClicked = function(e) {
    e.preventDefault();
    return this.collection.trigger('removeClicked', this.model);
  };

  return Li;

})(LabelsList.View);

LabelsList.Ul = (function(superClass) {
  extend(Ul, superClass);

  function Ul() {
    return Ul.__super__.constructor.apply(this, arguments);
  }

  Ul.prototype.tagName = 'ul';

  Ul.prototype.className = 'list-inline labels-list';

  Ul.prototype.initialize = function(options) {
    this.filter = options.filter;
    this.items = [];
    return this.listenTo(this.collection, 'sync', this.render);
  };

  Ul.prototype.render = function() {
    this.clearItems();
    this.renderCollection();
    return this;
  };

  Ul.prototype.renderCollection = function() {
    if (this.filter) {
      return _.each(this.collection.where(this.filter), (function(_this) {
        return function(model) {
          return _this.renderItem(model);
        };
      })(this));
    } else {
      return this.collection.each((function(_this) {
        return function(model) {
          return _this.renderItem(model);
        };
      })(this));
    }
  };

  Ul.prototype.renderItem = function(model) {
    var item;
    item = this.settings.makeLi({
      model: model,
      collection: this.collection
    });
    this.$el.append(item.render().$el);
    return this.items.push(item);
  };

  Ul.prototype.remove = function() {
    this.clearItems();
    return Ul.__super__.remove.apply(this, arguments);
  };

  Ul.prototype.clearItems = function() {
    _.each(this.items, function(view, key) {
      return view.remove();
    });
    return this.items = [];
  };

  return Ul;

})(LabelsList.View);

LabelsList.Settings = (function(superClass) {
  extend(Settings, superClass);

  function Settings() {
    return Settings.__super__.constructor.apply(this, arguments);
  }

  Settings.prototype.defaults = {
    LiConstructor: LabelsList.Li,
    removeLink: true
  };

  Settings.prototype.makeLi = function(options) {
    if (options == null) {
      options = {};
    }
    return this.makeViewInstance(this.get('LiConstructor'), options);
  };

  Settings.prototype.makeViewInstance = function(Constructor, options) {
    _.extend(options, {
      settings: this
    });
    return new Constructor(options);
  };

  return Settings;

})(Backbone.Model);
  return LabelsList;
}));